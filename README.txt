
Echopay Gateway for Ubercart

This module provides basic functionality to be able to process payments
using the Echopay gateway (http://www.echo-inc.com/) and Ubercart.

Prior to installing, you must download the EchoPHP class from Echo, (or have
their customer support send you a copy) and place echophp.class in the module's
directory.

After that, installation and configuration is standard for a gateway in 
Ubercart.  After enabling the module, go to 
admin/store/settings/payment/edit/gateways 
to enter your merchant Echo ID and PIN.  Then go to
admin/store/settings/payment/edit/methods 
to set it as the default gateway for the Credit card payment method.

Compatibility
This module is compatible with Ubercart 2.x in Drupal 6.x.  It has been tested
with version 1.7.1 of the EchoPHP class.

Notes:
The echophp.class file is directly from Echopay.  This module serves as a
wrapper so that Ubercart can make use of the class.  For more information about
Echopay's API, gateway account settings, or about testing transactions,
please go to http://www.echo-inc.com/.
